/*

Color Pallette:
 Electric Yellow: #Ffff33, rgb: 255,255,51

imgBB
 logo: https://ibb.co/y05Fjqv
 logo2: https://i.ibb.co/9YsBs9s
 crying-mt: https://ibb.co/121kmPm
 crying-mt2: https://ibb.co/dJ4Yvtg
 manoys: https://ibb.co/ZV8qc43

fonts
 'Phudu', cursive;
 'Quicksand', sans-serif;


= = = T I M E L I N E = = =

 20Feb-2023 Monday
 	AM	SESSION 59
 		 * Register Page.
 		 * Login Page.

 	PM	SESSION 60
 		 * ADMIN DASHBOARD PAGE
			- Create Product
			- Retrieve ALL Products.

 21Feb-2023 Tuesday
 	AM	SESSION 61
 		 * ADMIN DASHBOARD PAGE
			- Update Product Information
			- Deactivate/Reactivate Product.

 	PM	SESSION 62
 		 * USER PRODUCTS CATALOG PAGE
			- View ALL Active Products.
			- Retrieve Single Product.

 22Feb-2023 Wednesday
 	AM	SESSION 63
 		 * Initial deployment of Ecommerce app to Vercel
 		 * Checkout Order.
 		 	- Non Admin User Checkout (Create Order)
 		 * Stretch Goals.
	
 	PM	SESSION 64
 		 * PRESENTATION

 23Feb-2023 Thursday
 		SESSION 65 [ Last Session? :< ]
			  8-10PM * PRESENTATION (Part 2, if ever)
			 10-12NN * Career Road Map
			  1-2 PM * CAT Session & MKT Session
				2 PM * COMMENCEMENT


= = = R E Q U I R E M E N T S = = =

 MINIMUM REQUIREMENTS
	● Register Page
		- with proper form validation (all fields must be filled and passwords must match) that must redirect the user to the login page once successfully submitted
	● Login Page
		- must redirect the user to the either the home page or the products catalog once successfully authenticated
	● Products Catalog Page
	 	○ Retrieve all active products
	 	○ Retrieve single product
	● Admin Panel/Dashboard
	 	○ Create Product 
	 	○ Retrieve all products
	 	○ Update Product information 
	 	○ Deactivate/reactivate product
	● Checkout Order
	 	○ Non-admin User checkout (Create Order)

 OTHER REQUIREMENTS
 	● A fully-functioning Navbar with proper dynamic rendering 
 		(Register/Login links  for users not logged in, Logout link for users who are, etc)
 	● App must be single-page and utilize proper routing (no navigating to another  page/reloading)
 	● Registration/Login pages must be inaccessible to users who are logged-in 
 	● Apart from users who are not logged-in, ADMIN MUST "NOT" be able to add products to their cart
 	● Do not create a website other than the required e-commerce app
 	● Do not use templates found in other sites or existing premade NPM packagesn that replicate a required feature

 STRETCH GOALS
	● Full responsiveness across mobile/tablet/desktop screen sizes
	● Product images
	● A hot products/featured products section
	● View User Details (Profile) - You can change their password
	● SETTING USER AS ADMIN
	● "ADD TO CART" FEATURE
	● ORDER HISTORY
		○ Retrieve a list of all orders made by user
		○ Admin feature to retrieve a list of all orders made by all users


= = = N O T E S = = =

 CREATE REACT APP
	 npx create-react-app capstone-3

 REMOVE FILES AND ITS IMPORTATIONS
   FILES /src folder
	   - App.test.js
	   - index.css
	   - reportWebVitals.js
	   - logo.svg
   IMPORTATIONS 
     from "index.js"
	   - index.css
	   - repotwebvitals
     from "App.js"
	   - logo.svg

 VERIFY CONNECTION

 NPM INSTALL PACKAGES
  - react-bootstrap			* needed to use bootstrap
  - bootstrap						* to use bootstrap
  - react-router-dom		* needed for routing

  - sweetalert2					* (optional) for pop-up boxes
  - react-modal					* (optional) to use modal
  - mdb-react-ui-kit		* (optinal) footer etc

 CREATE .env
  - API DataBase (localhost:6000)
	- process.env.




*/