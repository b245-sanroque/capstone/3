import React from 'react';

// create userCOntext
const UserContext = React.createContext();

// logged-in user
export const UserProvider = UserContext.Provider;

export default UserContext;