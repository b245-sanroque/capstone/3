// imports
 import { Container , Nav , Navbar } from 'react-bootstrap';
 import { Offcanvas } from 'react-bootstrap';
 import { Link , NavLink } from 'react-router-dom';

// exports
export default function AppNavbar(){

  return(
	<Navbar bg="black" variant="dark" expand="lg" className="sticky-top">
	  <Container fluid>
			<img id="navbar-logo" className="img-fluid" src="https://i.ibb.co/BLj2MBW/electricitea-logo.jpg" alt="electricitea-logo"/>
			<Navbar.Brand as = {Link} to = "/" id="electricitea-text">ELECTRICITEA</Navbar.Brand>
			<Navbar.Toggle aria-controls="offcanvasNavbar-expand-lg" id="navbar-toggler"/>
				<Navbar.Offcanvas className="bg-dark text-muted" aria-labelledby="offcanvasNavbarLabel-expand-lg" placement="end">
		  		<Offcanvas.Header closeButton>
		    		<Offcanvas.Title id="offnav">ELECTRICITEA</Offcanvas.Title>
		  		</Offcanvas.Header>
		  		<Offcanvas.Body>
		    		<Nav className="justify-content-end flex-grow-1 ps-3">
						<Nav.Link as = {NavLink} to = "/about" id="about">ABOUT US</Nav.Link>
						<Nav.Link as = {NavLink} to = "/menu" id="menu">MENU</Nav.Link>
						<Nav.Link as = {NavLink} to = "/delivery" id="delivery">DELIVERY</Nav.Link>
						<Nav.Link as = {NavLink} to = "/location" id="location">LOCATION</Nav.Link>				
						<Nav.Link as = {NavLink} to = "/career" id="career">CAREER</Nav.Link>
						<Nav.Link as = {NavLink} to = "/contact" id="contact">CONTACT US</Nav.Link>
		    		</Nav>
		  		</Offcanvas.Body>
				</Navbar.Offcanvas>
	  </Container>
	</Navbar>  	
  )

}