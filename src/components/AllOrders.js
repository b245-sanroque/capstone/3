
import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext.js';

import OrderCard from "./OrderCard.js"

export default function AllOrders () {

    const [order, setOrder] = useState([]);
    const {user} = useContext(UserContext);

    useEffect(() => {

        fetch(`${process.env.REACT_APP_LOCAL}/order`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(data => {
            // console.log(data)
            setOrder(data.map(list => {
                // console.log(prof._id)
                return(
                    <OrderCard key = {list._id} orderProp = {list}/>
                    )
            }))
        })
    }, []);

    return(
        <Fragment>
        <Container fluid className="mt-5 mb-5">
          <Row>
            <Col>
            <h3 className = "text-center mt-3"> ORDERS </h3>
            {order}
            </Col>
          </Row>
        </Container>

        </Fragment>
        )
};
