import React from 'react';
import { Container , Row , Col } from 'react-bootstrap';
// import { Link , NavLink } from 'react-router-dom';
import { MDBFooter, MDBIcon, MDBBtn } from 'mdb-react-ui-kit';

export default function App() {
  return (
    <MDBFooter className='text-center text-lg-start' style={{ backgroundColor: '#ffff33' }}>
      <Container fluid className="text-center" style={{ backgroundColor: '#000000' }} >
        <Row>
          <Col md="6" lg="4" className="mx-auto">
            <img id="footer-logo" className="img-fluid" src="https://i.ibb.co/9YsBs9s/electricitea-logo2.jpg" alt="electricitea-logo"/>
          </Col>
          <Col id="footer-contact" md="6" lg="4" className="mx-auto text-uppercase mb-1">
            <h5 className='fw-bold m-4'>Contact Us</h5>
            <p>lot 1 blk. 5 NHA Ave.</p>
            <p>Dela Paz, Antipolo,</p>
            <p>Philippines, 1870</p>
            <section className="d-flex justify-content-center p-4">
              <MDBBtn floating className='mx-2' style={{ backgroundColor: '#3b5998' }} href='https://www.facebook.com/electricitea2019/' role='button' >
                <MDBIcon fab icon='facebook-f' />
              </MDBBtn>
              <MDBBtn floating className='mx-2' style={{ backgroundColor: '#55acee' }} href='https://www.twitter.com/electricitea2019/' role='button' >
                <MDBIcon fab icon='twitter' />
              </MDBBtn>
              <MDBBtn floating className='mx-2' style={{ backgroundColor: '#ac2bac' }} href='https://www.instagram.com/electricitea2019/' role='button' >
                <MDBIcon fab icon='instagram' />
              </MDBBtn>        
              <MDBBtn floating className='mx-2' style={{ backgroundColor: '#dd4b39' }} href='mailto:electricitea2019@gmail.com/' role='button' >
                <MDBIcon far icon='envelope' />
              </MDBBtn>
            </section>

          </Col>
          <Col md="8" lg="4" className="mx-auto mb-4">
          <img id="footer-logo2" className="img-fluid" src="https://i.ibb.co/KXqTV4Y/manoys-logo.jpg" alt="manoys-logo"/>
          </Col>
        </Row>
      </Container>
      <div className='text-center p-2' style={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}>
         <h5 className="footer-text fst-italic">Get #elecTEAfied</h5>
         <h5 className="footer-text fw-bold">© ALL RIGHTS RESERVED 2023</h5>
       </div>
    </MDBFooter>
  );
}