// imports
 import { Fragment , useContext ,useState , useEffect } from 'react';
 import { Row , Col , Card , Button , Modal , Form } from 'react-bootstrap';
 import { Link , useNavigate , useParams } from 'react-router-dom';
 
import Swal from 'sweetalert2';
 import UserContext from '../UserContext.js'

// export default function ItemCard({itemProp}){
export default function ProfileCard({profileProp}){
	//constsssss
	 const { _id , username , firstName , lastName , email ,isAdmin , mobileNo } = profileProp;
	 const {user} = useContext(UserContext);
	 const navigate = useNavigate();
	 const { userId } = useParams();

	 const [role, setRole] = useState("User")
	 // console.log(isAdmin)
	 useEffect(()=>{
		if(isAdmin === true){
			setRole("Admin")
		} else{
			setRole("User")
		}
	 } ,[])

	//setadmin
	 const setAdmin = (id) => {
		fetch(`${process.env.REACT_APP_LOCAL}/user/setAdmin/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}			
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			if(data.isAdmin === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: `${data.username} has successfully been made to be an Admin!` 
				})
			navigate(`/dashboard`);
			} else {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: `${data.username}'s Admin Role has been revoked!`
				})
			}
			navigate(`/dashboard`);
		}).catch(error => console.log(error))
	 }

	//resetpassword
	 const resetPass = (id) => {
		fetch(`${process.env.REACT_APP_LOCAL}/user/pwChange/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}			
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			if(data){
				Swal.fire({
					title: `Succesfully reset ${data.username}'s password`,
					icon: "success",
					text: "Tell them it's their username :>" 
				})
			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: `idk wth happened!`
				})
			}
			navigate(`/dashboard`);
		}).catch(error => console.log(error))
	 }

	//modal
	 const [show, setShow] = useState(false);
	 const handleClose = () => setShow(false);
	 const handleShow = () => setShow(true);

	 // change password
	 const [currentPassword, setCurrentPassword] = useState('');
	 const [newPassword, setNewPassword] = useState('');

	 function changePass(event){
	 	event.preventDefault();

 	  fetch(`${process.env.REACT_APP_LOCAL}/user/pwChange`, {
 		method: 'PUT',
 		headers: {
 		  'Content-Type' : 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
 		},
 		body: JSON.stringify({
 		  currentPassword: currentPassword,
 		  newPassword: newPassword
 		})
 	  }).then(result => result.json()
 	  ).then(data => {
 	  	console.log(data);
 	  	if(data){
 	  	  Swal.fire({
 	  		title: "Password Changed!",
 	  		icon: "success"
 	  	  })
 	  	  setShow(false);
 	  	  setNewPassword('')
 	  	  setCurrentPassword('')
 	  	} else{
 	  	  Swal.fire({
 	  		title: "Error!",
 	  		icon: "error",
 	  		text: "You may have entered a wrong password!"
 	  	  })
 	  	}	
 	  })
	 }
	  //capslock
	  const [isCapsLockOn, setIsCapsLockOn] = useState(false);
	  const checkCapsLock = (event) => {
	   if (event.getModifierState('CapsLock')) {
	     setIsCapsLockOn(true);
	   } else {
	     setIsCapsLockOn(false);
	   }
	  }

	return(
	<Row className = "mt-5">	
	  <Col>
      	<Card className = "card col-10 mx-auto">
		  <Card.Body>
			<Card.Title className="text-primary">{username}</Card.Title>
			<Card.Subtitle>Role:</Card.Subtitle>
			<Card.Text>{role}</Card.Text>
			<Card.Subtitle>Name:</Card.Subtitle>
			<Card.Text>{firstName} {lastName}</Card.Text> 
			<Card.Subtitle>Email:</Card.Subtitle>
			<Card.Text>{email}</Card.Text>
			<Card.Subtitle>Mobile Number:</Card.Subtitle>
			<Card.Text>{mobileNo}</Card.Text>
			{
				user.isAdmin ?
				<Fragment>
				<Button variant="info" className="m-2" as = {Link} to = {`/user/${_id}`}>View Profile</Button>
				{
					!isAdmin ?
					<Button variant="warning" className="m-2" onClick={() => setAdmin(userId)}>Set as Admin</Button>
					:
					<Button variant="danger" className="m-2" onClick={() => setAdmin(userId)}>Revoke Admin Role</Button>
				}
				<Button variant="primary" className="m-2" onClick={() => resetPass(userId)}>Reset Password</Button>
				</Fragment>
				:
				<Fragment>
				<Button variant="info" className="m-2" as = {Link} to = {`/user/${_id}`}>View Profile</Button>
				<Button variant="warning" className="m-2"> Update Profile </Button>			
				<Button variant="primary" onClick={handleShow}> Change Password </Button>
					<Modal show={show} onHide={handleClose}>
				      <Modal.Header closeButton>
				        <Modal.Title>Change Password</Modal.Title>
				      </Modal.Header>

				      <Modal.Body>
				        <Form >
				          <Form.Group className="mb-3">
				            <Form.Label className="fw-bold">Current Password</Form.Label>
				            <Form.Control
				                type="password"
				                placeholder="Current Password"
				                value = {currentPassword}
				                onKeyUp = { checkCapsLock }
				                onChange = {event => setCurrentPassword(event.target.value)}
				                required
				                autoFocus
				            />
			              </Form.Group>
			
			              <Form.Group className="mb-3">
		  	                <Form.Label className="fw-bold">New Password</Form.Label>
				            <Form.Control
				                type="password"
				                placeholder="New Password"
				                value = {newPassword}
				                onKeyUp = { checkCapsLock }
				                onChange = {event => setNewPassword(event.target.value)}
				                required
				                autoFocus
				            />
				              {isCapsLockOn && (
				               <p className = "text-danger">Caps Lock is ON</p>
				              )}
				          </Form.Group>
		                </Form>
				      </Modal.Body>
				      <Modal.Footer>
				        <Button variant="secondary" onClick={handleClose}> Close </Button>
				        <Button variant="primary" onClick={event => changePass(event)}> Save Changes </Button>
				      </Modal.Footer>
				    </Modal>
				</Fragment>
			}
		  </Card.Body>
		</Card>
	  </Col>
	</Row>
  )
}
