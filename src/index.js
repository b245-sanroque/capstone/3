import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// import bootstrap css
 import 'bootstrap/dist/css/bootstrap.min.css';

// mdb react
 import 'mdb-react-ui-kit/dist/css/mdb.min.css';
 import "@fortawesome/fontawesome-free/css/all.min.css";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*
//VERIFICATION OF CONNECTION
const wordszz = "is this working?";
const testing = <h1 className = "text-center mt-5">uhmmm, {wordszz}!</h1>;

root.render(
    testing
)
*/