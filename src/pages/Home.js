
import { Fragment } from 'react';
import { Container} from 'react-bootstrap';
import NavbarUser from '../components/NavbarUser.js';

export default function Home(){

	return(
		<Fragment>
		  <NavbarUser />
	  	  <Container fluid id="home-container">	
	  	  
	  	  </Container>
		</Fragment>
	)

}