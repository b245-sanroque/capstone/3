
import { Fragment } from 'react';
import { Container , Row , Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import NavbarUser from '../components/NavbarUser.js';

export default function Career() {
  
  return (
  	<Fragment>
  	<NavbarUser/>
  	  <Container fluid className='text-center p-2' style={{ backgroundColor: 'rgba(255, 255, 51, 0.50)' }}>
  	  	<Row id="career-page">
  	  		<Col>
	  	  		<img id="career-img" className="img-fluid"src="https://i.ibb.co/GFsb0Py/electricitea-hiring.jpg" alt="hiring"/>
	  	  	</Col>
	  	</Row>
  	  </Container>
    </Fragment>
  )

}