import { Container , Accordion } from 'react-bootstrap';
import { Fragment } from 'react';

import AllProducts from '../components/AllProducts.js';
import AllProfile from '../components/AllProfile.js';
import AllOrders from '../components/AllOrders.js';

export default function Dashboard () {

	return(	
	<Fragment>
		  <Container id="dashboard-container" className="mt-5 mb-5">
		  	<h1 className="dashboard-text text-center mb-5 mt-3"> ADMIN DASHBOARD </h1>
		    <Accordion className=" accordion px-5">

			  <Accordion.Item eventKey="0">
		        <Accordion.Header>View ALL Products</Accordion.Header>
		          <Accordion.Body>
		              <AllProducts />
		          </Accordion.Body>
		      </Accordion.Item>

		      <Accordion.Item eventKey="1">
		        <Accordion.Header>View ALL Profiles</Accordion.Header>
		          <Accordion.Body>
		              <AllProfile />
		          </Accordion.Body>
		      </Accordion.Item>

		      <Accordion.Item eventKey="2">
		        <Accordion.Header>View ALL Orders</Accordion.Header>
		          <Accordion.Body>
		              <AllOrders />
		          </Accordion.Body>
		      </Accordion.Item>
		    </Accordion>
		  </Container>
		</Fragment>
	)
}
