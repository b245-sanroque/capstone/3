import { Container } from 'react-bootstrap';
import { Fragment } from 'react';

import AllOrders from '../components/AllOrders.js';
import NavbarUser from '../components/NavbarUser.js';

export default function Orders () {

	return(	
	<Fragment>
		<NavbarUser />
		  <Container fluid className="mt-5 mb-5">
		  <AllOrders />
		  </Container>
	</Fragment>
	)

}